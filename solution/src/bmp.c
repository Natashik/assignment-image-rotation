#include <memory.h>
#include <stdlib.h>

#include "bmp.h"

#define FILE_HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define BMP_HEADER_IDENTITY 0x4D42
#define PLANES_COUNT 1
#define BITS_PER_PIXEL 24
#define ZERO 0

static int8_t getPadding(struct image const *img) {
    return (int8_t)((4 - ((img->width * sizeof(struct pixel)) % 4)) % 4);
}

static bool checkSignature(struct bmpFileHeader header) {
    return header.type == BMP_HEADER_IDENTITY;
}

static struct bmpFileHeader getBmpFileHeader(struct image const* img) {
    const int32_t FILE_SIZE = FILE_HEADER_SIZE + INFO_HEADER_SIZE + sizeof(struct pixel) * img->width * img->height;
    struct bmpFileHeader file_header;

    file_header.type = BMP_HEADER_IDENTITY; // BM in hex
    file_header.size = FILE_SIZE;
    file_header.reserved1 = 0;
    file_header.reserved2 = 0;
    file_header.offset = FILE_HEADER_SIZE + INFO_HEADER_SIZE;

    return file_header;
}

static struct bmpInfoHeader getBmpInfoHeader(struct image const* img) {
    struct bmpInfoHeader info_header;

    info_header.size = INFO_HEADER_SIZE;
    info_header.width = img->width;
    info_header.height = img->height;
    info_header.planes = PLANES_COUNT;
    info_header.bits_per_pixel = BITS_PER_PIXEL;
    info_header.compression = ZERO;
    info_header.image_size = ZERO;
    info_header.horizontal_resolution = ZERO;
    info_header.vertical_resolution = ZERO;
    info_header.colors_used = ZERO;
    info_header.colors_important = ZERO;

    return info_header;
}

enum readStatus fromBmp(FILE *in, struct image *img) {
    struct bmpFileHeader file_header;
    struct bmpInfoHeader info_header;

    if (!fread(&file_header, sizeof(file_header), 1, in)) {
        return READ_INVALID_HEADER;
    }

    if (!checkSignature(file_header)) {
        return READ_INVALID_SIGNATURE;
    }

    if (!fread(&info_header, sizeof(info_header), 1, in)) {
        return READ_INVALID_HEADER;
    }

    *img = CreateImage(info_header.width, info_header.height);
    const int8_t PADDING = getPadding(img);

    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            struct pixel pixel;
            if (!fread(&pixel, sizeof(struct pixel), 1, in)) {
                return READ_INVALID_BITS;
            }
            img->data[i * img->width + j] = pixel;
        }

        if (fseek(in, PADDING, SEEK_CUR)) {
            return READ_INVALID_SIGNATURE;
        }
    }

    return READ_OK;
}

enum writeStatus toBmp(FILE *out, struct image const *img) {
    static const uint8_t bmp_pad[3] = {0, 0, 0};
    const int8_t PADDING = getPadding(img);

    struct bmpFileHeader file_header = getBmpFileHeader(img);
    struct bmpInfoHeader info_header = getBmpInfoHeader(img);

    if (!fwrite(&file_header, sizeof(struct bmpFileHeader), 1, out) ||
            !fwrite(&info_header, sizeof(struct bmpInfoHeader), 1, out)) {
        return WRITE_ERROR;
    }

    for (int i = 0; i < img->height; ++i) {
        for (int j = 0; j < img->width; ++j) {
            struct pixel pixel = img->data[i * img->width + j];
            if (!fwrite(&pixel, sizeof(struct pixel), 1, out)) {
                return WRITE_ERROR;
            }
        }

        if (!fwrite(bmp_pad, sizeof(uint8_t), PADDING, out)) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}


void rotateBmp(const char *source, const char *dest) {
    FILE *source_file = fopen(source, "r");

    if (!source_file) {
        fprintf(stderr, "Unable to source file");
        exit(EXIT_FAILURE);
    }

    FILE *dest_file = fopen(dest, "w");

    if (!dest_file) {
        fclose(source_file);
        fprintf(stderr, "Unable to destination files");
        exit(EXIT_FAILURE);
    }

    struct image image;
    enum readStatus read_status = fromBmp(source_file, &image);
    if (read_status != READ_OK) {
        fprintf(stderr, "%s", bmp_read_error_msg[read_status]);
        exit(EXIT_FAILURE);
    }

    struct image rotated_image = rotateImage(image);
    enum writeStatus write_status = toBmp(dest_file, &rotated_image);
    if (write_status != WRITE_OK) {
        if (image.data) free(image.data);
        if (rotated_image.data) free(rotated_image.data);
        fprintf(stderr, "%s", bmp_read_write_msg[write_status]);
        exit(EXIT_FAILURE);
    }

    if (image.data)
        free(image.data);

    if (rotated_image.data)
        free(rotated_image.data);

    fclose(dest_file);
    fclose(source_file);
}
