#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect arguments!");
        return EXIT_FAILURE;
    }

    const char* source = argv[1];
    const char* dest = argv[2];

    rotateBmp(source, dest);

    return EXIT_SUCCESS;
}
