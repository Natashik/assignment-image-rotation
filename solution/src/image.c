#include <stdio.h>
#include <stdlib.h>

#include "image.h"

struct image CreateImage(uint32_t width, uint32_t height) {
    struct image image;

    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * width * height);

    if (!image.data) {
        fprintf(stderr, "Can't alloc enough memory");
        exit(EXIT_FAILURE);
    }

    return image;
}

struct image rotateImage(struct image const source) {
    struct image image = CreateImage(source.height, source.width);

    for (int i = 0; i < source.height; ++i) {
        for (int j = 0; j < source.width; ++j) {
            image.data[(source.width - j - 1) * source.height + i] = source.data[
                    (source.height - i - 1) * source.width + (source.width - j - 1)];
        }
    }

    return image;
}
