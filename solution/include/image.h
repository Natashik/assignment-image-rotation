#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include  <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image CreateImage(uint32_t width, uint32_t height);

struct image rotateImage(struct image const source);

#endif //IMAGE_TRANSFORMER_IMAGE_H
