#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include <stdbool.h>
#include  <stdint.h>
#include  <stdio.h>

#include "image.h"

#pragma pack(push, 1)
struct bmpFileHeader {
    uint16_t type;
    uint32_t size;
    uint16_t reserved1;
    uint16_t reserved2;
    uint32_t offset;
};

struct bmpInfoHeader {
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t image_size;
    int32_t horizontal_resolution;
    int32_t vertical_resolution;
    uint32_t colors_used;
    uint32_t colors_important;
};
#pragma pack(pop)

enum readStatus {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NOT_ENOUGH_MEMORY
};

enum writeStatus {
    WRITE_OK = 0,
    WRITE_ERROR
};

static const char *const bmp_read_error_msg[] = {
        [READ_INVALID_SIGNATURE] = "BMP file has invalid signature",
        [READ_INVALID_BITS] = "BMP file has invalid pixel array",
        [READ_INVALID_HEADER] = "BMP file has invalid header"};

static const char *const bmp_read_write_msg[] = {
        [WRITE_ERROR] = "Error occurred during writing in file"};

enum writeStatus toBmp(FILE *out, struct image const *img);

enum readStatus fromBmp(FILE *in, struct image *img);

void rotateBmp(const char *source, const char *dest);

#endif //IMAGE_TRANSFORMER_BMP_H
